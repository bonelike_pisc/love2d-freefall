require("mod.state_chain")
require("mod.initial")
require("mod.freefall")

state = StateChain.new(Initial.new(FreeFall.new(20.0)))

function love.load()
    --love.window.setMode(768, 432)
end

function love.draw()
    if state.terminated then
        love.graphics.setColor(255,255,255)
        love.graphics.setNewFont(20)
        love.graphics.print("THE END", 300, 200)
    else
        state:draw()
    end
end

function love.update(dt)
    state:update(dt)
end

function love.keypressed(key, scancode, isrepeat)
    if state.terminated or key == "escape" then
        love.event.quit()
    end

    state:keypressed(key, scancode, isrepeat)
end
