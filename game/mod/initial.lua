Initial = {}

function Initial.init(self)
    love.graphics.setBackgroundColor(0, 0, 0.25)
    love.graphics.setNewFont(20)
    love.graphics.setColor(255,255,255)
end

function Initial.draw(self)
    love.graphics.print("Press Space Key.", 250, 200)
end

--function Initial.update(self, dt)
    --何もしない
--end

function Initial.keypressed(self, key, scancode, isrepeat)
    if key == 'space' then
        self.go_next = true
    end
end

function Initial.next(self)
    if self.go_next then
        return self.next_state
    else
        return nil
    end
end

function Initial.new(next_state)
    local _o = {
        go_next = false,
        next_state = next_state
    }
    return setmetatable(_o, {__index = Initial})
end
