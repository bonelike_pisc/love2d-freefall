FreeFall = {}

function FreeFall.init(self)
    self.image = love.graphics.newImage("resource/apple.png")
end

function FreeFall.draw(self)
    love.graphics.draw(self.image, self.x, self.y, 0, self.scale)
end

function FreeFall.update(self, dt)
    self.vy = self.vy + self.g * dt
    self.y = self.y + self.vy * dt
end

function FreeFall.next(self)
    if self.y > love.graphics.getHeight() - self.image:getHeight() * self.scale then
        return "quit"
    else
        return nil
    end
end

function FreeFall.new(g)
    _o = {
        g = g,
        vy = 0,
        x = 300.0,
        y = 0.0,
        scale = 2.0,
        image = nil
    }
    return setmetatable(_o, {__index = FreeFall})
end
