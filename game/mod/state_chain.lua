StateChain = {}

function StateChain.draw(self)
    if self.inner_state.draw ~= nil then
        self.inner_state:draw()
    end
end

function StateChain.update(self, dt)
    if self.terminated then
        return
    end

    if not self.initialized then
        self.inner_state:init()
    end

    if self.inner_state.update ~= nil then
        self.inner_state:update(dt)
    end

    local _next = self.inner_state:next()
    if _next == "quit" then
        self.terminated = true
    elseif _next ~= nil then
        self.inner_state = _next
        self.inner_state:init()
    end
end

function StateChain.keypressed(self, key, scancode, isrepeat)
    if self.inner_state.keypressed ~= nil then
        self.inner_state:keypressed(key, scancode, isrepeat)
    end
end

function StateChain.new(initial_state)
    local _obj = {
        inner_state = initial_state,
        initialized = false,
        terminated = false
    }
    return setmetatable(_obj, {__index = StateChain})
end
